package com.weichai.app.arch.http;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @ClassName: ApiManager
 * @Description: 网络请求封装
 * @Author: 祖安
 * @Date: 2021/7/19 10:56 上午
 */
public class ApiManager {
    private static final String BASE_URL = "http://weichai.amdu.dtyunxi.cn/api/";
    private Retrofit retrofit;
    private ApiServer mApiServer;
    private OkHttpClient mOkHttpClient;

    public ApiManager() {
        init();
    }

    private void init() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        //拦截日志
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(new CommonParamsInterceptor());
        builder.addInterceptor(interceptor);
        mOkHttpClient = builder.build();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create()) // 支持RxJava
                .client(mOkHttpClient)
                .build();
        mApiServer = retrofit.create(ApiServer.class);
    }

    static public ApiServer getInstance() {
        return SingleOn.instance.mApiServer;
    }

    static class SingleOn {
        public static ApiManager instance = new ApiManager();
    }
}