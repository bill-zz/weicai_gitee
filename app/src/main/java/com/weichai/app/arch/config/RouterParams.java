package com.weichai.app.arch.config;

import android.os.Bundle;

import androidx.annotation.IdRes;

/**
 * @ClassName: RouterParam
 * @Description: 路由跳转参数
 * @Author: 祖安
 * @Date: 2021/8/7 10:57 下午
 */
public class RouterParams {
    public @IdRes
    int pageID;//跳转页面id(navigation 配置清单)
    public Bundle params;///参数传递

    public RouterParams(int pageID, Bundle params) {
        this.pageID = pageID;
        this.params = params;
    }

    public RouterParams(int pageID) {
        this.pageID = pageID;
        this.params = params;
    }
}
