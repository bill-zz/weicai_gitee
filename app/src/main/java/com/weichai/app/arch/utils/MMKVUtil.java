package com.weichai.app.arch.utils;

import android.os.Parcelable;

import androidx.annotation.Nullable;

import com.tencent.mmkv.MMKV;

import java.util.Set;

/**
 * @ClassName: MMKVUtil
 * @Description:
 * @Author: 祖安
 * @Date: 2021/11/18 10:56 上午
 */
public class MMKVUtil {

    public static MMKV instance(){
        return MMKV.defaultMMKV();
    }

    public static void save(String key, String value){
        instance().encode(key, value);
    }

    public static void save(String key, boolean value){
        instance().encode(key, value);
    }

    public static void save(String key, byte[] value){
        instance().encode(key, value);
    }

    public static void save(String key, int value){
        instance().encode(key, value);
    }

    public static void save(String key, long value){
        instance().encode(key, value);
    }

    public static void save(String key, float value){
        instance().encode(key, value);
    }

    public static void save(String key, double value){
        instance().encode(key, value);
    }

    public static void save(String key, Set<String> value){
        instance().encode(key, value);
    }

    public static void save(String key, @Nullable Parcelable value){
        if (value == null){
            instance().removeValueForKey(key);
        }else {
            instance().encode(key, value);
        }
    }

    public static String getString(String key){
        return instance().decodeString(key);
    }
    public static String getString(String key,String defaultValue){
        return instance().decodeString(key,defaultValue);
    }

    public static boolean getBoolean(String key){
        return instance().decodeBool(key);
    }

    public static boolean getBoolean(String key, boolean defaultValue){
        return instance().decodeBool(key, defaultValue);
    }

    public static int getInteger(String key){
        return instance().decodeInt(key);
    }
    public static int getInteger(String key,int defaultValue){
        return instance().decodeInt(key,defaultValue);
    }


    public static long getLong(String key){
        return instance().decodeLong(key);
    }
    public static long getLong(String key, long defaultValue){
        return instance().decodeLong(key, defaultValue);
    }
    public static float getFloat(String key){
        return instance().decodeFloat(key);
    }

    public static double getDouble(String key){
        return instance().decodeDouble(key);
    }

    public static byte[] getBytes(String key){
        return instance().decodeBytes(key);
    }

    public static Set<String> getStringSet(String key){
        return instance().decodeStringSet(key);
    }

    public static <T extends Parcelable> T getParcelable(String key, Class<T> classType){
        try {
            return instance().decodeParcelable(key, classType);
        }catch (Exception e){
            return null;
        }
    }
}
