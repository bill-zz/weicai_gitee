package com.weichai.app.arch.http;


import android.util.Log;

import androidx.lifecycle.ViewModelProvider;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import retrofit2.adapter.rxjava3.HttpException;

import com.weichai.app.App;
import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.bean.BaseResult;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * @ClassName: ApiSubscriber
 * @Description: 请求统一订阅处理
 * @Author: 祖安
 * @Date: 2021/11/15 10:45 下午
 */
public abstract class ApiSubscriber<T> implements Observer<BaseResult<T>>, Disposable {
    boolean showErr = false;
    boolean showLoading = false;
    BaseViewModel viewModel;

    public ApiSubscriber(boolean showErr, boolean showLoading, BaseViewModel viewModel) {
        this.showErr = showErr;
        this.showLoading = showLoading;
        this.viewModel = viewModel;
    }


    public ApiSubscriber() {

    }

    @Override
    public void onNext(BaseResult<T> data) {
        onSuccess(data);
    }

    @Override
    public void onError(Throwable throwable) {
        ApiException apiException = null;

        if (throwable instanceof HttpException) {
            HttpException exception = (HttpException) throwable;
            apiException = new ApiException(String.valueOf(exception.code()), String.format("服务器异常：%s", exception.code()));
            if (exception.code() == 401) {//登录失效
                new ViewModelProvider(App.getApp())
                        .get(ApiExceptionVM.class)
                        .apiExceptionData
                        .postValue(apiException);
                return;
            }
        }
        if (throwable instanceof UnknownHostException) {
            apiException = new ApiException("-1", "网络异常：请检查网络后重试");
        }

        if (throwable instanceof SocketTimeoutException) {
            apiException = new ApiException("-1", "网络异常：网络超时");
        }

        if (throwable instanceof ApiException) {
            apiException = (ApiException) throwable;
        }

        if (apiException == null) {
            apiException = new ApiException("-1", String.format("未知异常：%s", throwable.getMessage()));
        }

        onFail(apiException);
        onComplete();
    }

    @Override
    public void onComplete() {
        if (viewModel != null && showLoading) {
            viewModel.showLoading(false);
        }
    }


    @Override
    public void dispose() {

    }

    @Override
    public boolean isDisposed() {
        return false;
    }

    @Override
    public void onSubscribe(@NonNull Disposable d) {
        if (viewModel != null && showLoading) {
            viewModel.showLoading(true);
        }
    }

    //请求成功回调
    public abstract void onSuccess(BaseResult<T> result);

    //失败回调
    public void onFail(ApiException apiException) {
        if (viewModel != null && showErr) {
            viewModel.showToast(apiException.msg);
        }
    }
}
