package com.weichai.app.arch.http;

import com.kunminx.architecture.ui.callback.UnPeekLiveData;
import com.weichai.app.arch.base.BaseViewModel;

/**
 * @ClassName: ApiExceptionVM
 * @Description:
 * @Author: asanant
 * @Date: 2021/12/1 9:10 下午
 */
public class ApiExceptionVM extends BaseViewModel {
    public UnPeekLiveData<ApiException> apiExceptionData = new UnPeekLiveData<>();//网络异常监听
}
