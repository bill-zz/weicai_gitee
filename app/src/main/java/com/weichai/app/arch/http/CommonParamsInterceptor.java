package com.weichai.app.arch.http;

import android.util.Log;

import androidx.annotation.NonNull;

import com.weichai.app.BuildConfig;
import com.weichai.app.arch.config.CacheConstant;
import com.weichai.app.arch.utils.MMKVUtil;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @ClassName: CommonParamsInterceptor
 * @Description: 公共参数设置
 * @Author: asanant
 * @Date: 2021/11/18 9:59 下午
 */
public class CommonParamsInterceptor implements Interceptor {
    @NonNull
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        String userToken = MMKVUtil.getString(CacheConstant.KEY_USER_TOKEN, "");
        Request request = chain.request()
                .newBuilder()
                .addHeader("Access-Token", userToken)//用户token
                .addHeader("weyStatus", "android")
                .addHeader("version", BuildConfig.VERSION_NAME)
                .build();
        return chain.proceed(request);
    }
}
