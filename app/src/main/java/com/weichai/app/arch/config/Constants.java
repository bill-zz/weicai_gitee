package com.weichai.app.arch.config;

public class Constants {

    /**
     * 更多
     */
    public static final int MORE = -1;

    /**
     * 客服电话
     */
    public static final int CUSTOMER_SERVICE_PHONE = 1;

    /**
     * 立即维修
     */
    public static final int REPAIR_IMMEDIATELY = 2;

    /**
     * 服务站
     */
    public static final int SERVICE_STATION = 3;

    /**
     * 发动机
     */
    public static final int ENGINE = 4;

    /**
     * 潍柴商城
     */
    public static final int MALL = 5;

    /**
     * 动力学院
     */
    public static final int SCHOOL_OF_DYNAMICS = 6;

    /**
     * 智多行
     */
    public static final int WISDOM_MORE_LINE = 7;

    /**
     * 故障码查询
     */
    public static final int FAULT_CODE_QUERY = 8;

    /**
     * 维保服务
     */
    public static final int MAINTENANCE_SERVICE = 9;

    /**
     * 门店位置
     */
    public static final int STORE_LOCATION = 10;
}
