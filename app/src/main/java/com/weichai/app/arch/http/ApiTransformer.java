package com.weichai.app.arch.http;

import androidx.annotation.NonNull;


import com.weichai.app.bean.BaseResult;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.core.ObservableTransformer;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;

/**
 * @ClassName: ObservableTransformer
 * @Description:
 * @Author: 祖安
 * @Date: 2021/7/22 4:00 下午
 */
public class ApiTransformer<T> implements ObservableTransformer<BaseResult<T>, BaseResult<T>> {


    public ApiTransformer() {
    }


    @NonNull
    @Override
    public ObservableSource<BaseResult<T>> apply(Observable<BaseResult<T>> upstream) {
        return upstream
                .subscribeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(baseResult -> {
                    if (!baseResult.resultCode.equals("0")) {
                        throw new ApiException(baseResult.resultCode, baseResult.resultMsg);
                    }
                });
    }
}