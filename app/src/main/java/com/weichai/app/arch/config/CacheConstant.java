package com.weichai.app.arch.config;

/**
 * @ClassName: CacheConstant
 * @Description:
 * @Author: 祖安
 * @Date: 2021/11/18 10:14 下午
 */
public class CacheConstant {
    public static final String KEY_USER_TOKEN = "key_user_token"; //用户token
    public static final String KEY_LOGIN_OUT_STATE = "key_user_state"; //用户退登录状态 1：正常退出 2：（401退出）

    /**
     * 搜索历史
     */
    public static final String KEY_SEARCH_HISTORY = "key_search_history";

    /**
     * 首页应用
     */
    public static final String KEY_ENTRANCE_HOME = "key_entrance_home";

    /**
     * 服务
     */
    public static final String KEY_ENTRANCE_SERVICE = "key_entrance_service";
    public static final String KEY_USER_INFO = "key_user_info";
}
