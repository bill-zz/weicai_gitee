package com.weichai.app.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.weichai.app.R;
import com.weichai.app.databinding.DialogEditBinding;

/**
 * create by bill on 8.12.21
 */
public class EditDialog extends Dialog {

    public EditDialog(@NonNull Context context) {
        super(context);
    }

    public EditDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected EditDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }


    private String title;
    private String msgHint;
    private int maxSize;

    private DialogEditBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(false);

        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_edit, null);
        dataBinding = DialogEditBinding.bind(view);
        setContentView(dataBinding.getRoot());

        WindowManager.LayoutParams params = getWindow().getAttributes();
        getWindow().getDecorView().setPadding(0, 0, 0, 0);
        getWindow().getDecorView().setBackgroundColor(Color.TRANSPARENT);
        params.gravity = Gravity.CENTER;
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(params);


        dataBinding.etMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.i("weichai", "onTextChanged: " + s.length());
                dataBinding.tvMaxSize.setText(count + "/" + maxSize);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        findViewById(R.id.tv_cancel).setOnClickListener(v -> dismiss());
        findViewById(R.id.tv_ok).setOnClickListener(v -> {
            dismiss();
            if (clickOk != null) {
                clickOk.onClickOk(dataBinding.etMsg.getText().toString().trim());
            }
        });
    }

    public EditDialog setTitle(String title) {
        this.title = title;
        return this;
    }

    public EditDialog setMaxSize(int maxSize) {
        this.maxSize = maxSize;
        return this;
    }

    public EditDialog setMsgHint(String hint) {
        this.msgHint = hint;
        return this;
    }

    @Override
    public void show() {
        super.show();

        dataBinding.tvTitle.setText(title);
        dataBinding.etMsg.setHint(msgHint);

        InputFilter[] filters = {new InputFilter.LengthFilter(maxSize)};
        dataBinding.tvMaxSize.setFilters(filters);
    }

    public interface IClickOk {
        void onClickOk(String msg);
    }

    private IClickOk clickOk;

    public void setClickOkCallback(IClickOk iClickOk) {
        clickOk = iClickOk;
    }
}
