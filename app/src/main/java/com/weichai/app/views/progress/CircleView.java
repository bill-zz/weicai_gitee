package com.weichai.app.views.progress;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.weichai.app.R;


public class CircleView extends View {
    public CircleView(Context context) {
        super(context);
        init();
    }

    public CircleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public CircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private int progressPercent = 50;
    private float strokeWidth;

    private Paint paint = new Paint();

    private void init() {
        paint.setAntiAlias(true);
        strokeWidth = getContext().getResources().getDimension(R.dimen.dp_10);
        paint.setStrokeWidth(strokeWidth);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
    }


    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        int centerX = getWidth() / 2;

        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setAntiAlias(true);
        paint.setDither(true);
        RectF rectF = new RectF(strokeWidth / 2, strokeWidth / 2, 2 * centerX - strokeWidth / 2, 2 * centerX - strokeWidth / 2);
        paint.setColor(Color.parseColor("#272727"));
        canvas.drawArc(rectF, 0, 360, false, paint);
        paint.setColor(Color.parseColor("#ffff8f43"));
        canvas.drawArc(rectF, -90, 360 * progressPercent / 100, false, paint);
    }


    public void setProgress(int progress) {
        this.progressPercent = progress;
        invalidate();
    }
}
