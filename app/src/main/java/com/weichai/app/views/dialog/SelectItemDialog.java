package com.weichai.app.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.weichai.app.R;
import com.weichai.app.bean.DictRes;
import com.weichai.app.bean.SelectItemEntity;
import com.weichai.app.databinding.DialogSelectBinding;
import com.weichai.app.databinding.DialogUserTypeBinding;
import com.weichai.app.views.dialog.adapter.SelectItemAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * create by bill on 8.12.21
 */
public class SelectItemDialog extends Dialog {

    public SelectItemDialog(@NonNull Context context) {
        super(context);
    }

    public SelectItemDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected SelectItemDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    private DialogSelectBinding dataBinding;


    private SelectItemAdapter adapter = new SelectItemAdapter();

    private String title;
    private List<DictRes> dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_select, null, false);
        dataBinding = DialogSelectBinding.bind(view);
        setContentView(dataBinding.getRoot());


        WindowManager.LayoutParams params = getWindow().getAttributes();
        getWindow().getDecorView().setPadding(0, 0, 0, 0);
        getWindow().getDecorView().setBackgroundColor(Color.parseColor("#00ffffff"));
        params.gravity = Gravity.BOTTOM;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(params);

        dataBinding.rv.setLayoutManager(new LinearLayoutManager(getContext()));
        dataBinding.rv.setAdapter(adapter);

        dataBinding.tvOk.setOnClickListener(v -> {
            dismiss();
            if (select != null) {
                select.onSelectClick(adapter.dictRes);
            }
        });

        dataBinding.ivClose.setOnClickListener(v -> {
            dismiss();
        });

        test();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setData(List<DictRes> dataList) {
        this.dataList = dataList;
    }

    private void test() {
        ArrayList<DictRes> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            DictRes dictRes = new DictRes();
            dictRes.setValue("a" + i);
            list.add(dictRes);
        }

        adapter.setList(list);
    }


    @Override
    public void show() {
        super.show();
        dataBinding.tvTitle.setText(title);
//        adapter.setList(dataList);

    }

    public interface ISelect {
        void onSelectClick(DictRes dictRes);
    }

    private ISelect select;

    public void setClickOkCallback(ISelect select) {
        this.select = select;
    }
}
