package com.weichai.app.views.dialog.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.weichai.app.R;
import com.weichai.app.bean.DictRes;
import com.weichai.app.bean.SelectItemEntity;
import com.weichai.app.databinding.ItemSelectMsgBinding;

/**
 * create by bill on 11.12.21
 */
public class SelectItemAdapter extends BaseQuickAdapter<DictRes, BaseDataBindingHolder<ItemSelectMsgBinding>> {
    public SelectItemAdapter() {
        super(R.layout.item_select_msg);
    }

    public DictRes dictRes;

    @Override
    protected void convert(@NonNull BaseDataBindingHolder<ItemSelectMsgBinding> holder, DictRes dict) {
        ItemSelectMsgBinding dataBinding = holder.getDataBinding();
        if (dataBinding != null) {
            dataBinding.setEntity(dict);
            dataBinding.clSelect.setOnClickListener(v -> {
                dictRes = dict;
                for (DictRes entity : getData()) {
                    entity.setSelect(entity == dict);
                }
            });
        }
    }

}
