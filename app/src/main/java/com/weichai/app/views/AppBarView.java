package com.weichai.app.views;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.weichai.app.R;

import dev.utils.app.BarUtils;

/**
 * @ClassName: AppBar
 * @Description: 导航栏
 * @Author: 祖安
 * @Date: 2021/9/1 1:53 下午
 */
public class AppBarView extends FrameLayout {
    private static final String TAG = "AppBarView";
    private int mAppBarColor = 0xFF333333;
    private ImageView mBackBtn;
    private ImageView mIvBg;
    private TextView mTvAppBarName;
    private TextView mTvRightText;
    private FrameLayout mFlActions;
    private String mAppBarTitle;
    private String mAppBarRightText;
    private @DrawableRes int mBtnBackIcon; ///左边按钮图标
    private OnClickListener mLeftBtnOnClickListener;
    private OnClickListener mRightTextOnClickListener;
    private float mBgAlpha = 1.0f;//AppBar背景透明度
    private boolean mAppbarTitleVisible;
    private boolean mShowBackBtn;//是否展示返回按钮


    public AppBarView(@NonNull Context context) {
        this(context, null);
    }

    public AppBarView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AppBarView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public AppBarView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initAttr(context, attrs);
        initView(context);
    }

    private void initAttr(Context context, @Nullable AttributeSet attrs) {
        if (attrs != null) {
            TypedArray tArray = context.obtainStyledAttributes(attrs, R.styleable.app_bar);//获取配置属性
            mAppBarColor = tArray.getColor(R.styleable.app_bar_appbar_color, mAppBarColor);
            mAppBarTitle = tArray.getString(R.styleable.app_bar_appbar_title);
            mAppBarRightText = tArray.getString(R.styleable.app_bar_appbar_right_text);
            mBgAlpha = tArray.getFloat(R.styleable.app_bar_appbar_alpha, 1);
            mAppbarTitleVisible = tArray.getBoolean(R.styleable.app_bar_appbar_title_visible, true);
            mShowBackBtn = tArray.getBoolean(R.styleable.app_bar_appbar_show_back, true);
            mBtnBackIcon = tArray.getResourceId(R.styleable.app_bar_appbar_icon, R.mipmap.ic_btn_back);
            tArray.recycle();
        }

    }


    /**
     * 初始化视图
     *
     * @param context
     */
    private void initView(Context context) {
        BarUtils.setStatusBarLightMode((Activity) context, true);
        ///绑定布局
        LayoutInflater.from(context).inflate(R.layout.app_bar, this);
        //适配状态栏高度
        setPadding(getLeft(), getTop() + BarUtils.getStatusBarHeight(), getRight(), getBottom());
        mBackBtn = findViewById(R.id.left_btn);
        mIvBg = findViewById(R.id.iv_bg);
        mTvAppBarName = findViewById(R.id.app_bar_name);
        mTvRightText = findViewById(R.id.right_text);
        setAppBarTitle(mAppBarTitle);
        setAppBarRightText(mAppBarRightText);
        setAppBarColor(mAppBarColor);
        setAppBarBgAlpha(mBgAlpha);
        setAppBarTitleVisible(mAppbarTitleVisible ? View.VISIBLE : View.GONE);
        mBackBtn.setImageResource(mBtnBackIcon);
        mBackBtn.setVisibility(mShowBackBtn ? View.VISIBLE : View.GONE);
        findViewById(R.id.left_fl).setOnClickListener(view -> {
            ///如果重新定义左边按钮点击事件则取消默认返回的事件
            if (mLeftBtnOnClickListener != null) {
                mLeftBtnOnClickListener.onClick(view);
            } else {
                if (context instanceof Activity) {
                    ((Activity) context).onBackPressed();
                }
            }
        });

        mTvRightText.setOnClickListener(v -> {
            if (null != mRightTextOnClickListener) {
                mRightTextOnClickListener.onClick(v);
            }
        });
    }

    public void setAppBarRightText(String mAppBarRightText) {
        mTvRightText.setText(mAppBarRightText);
    }

    /**
     * 设置标题栏文字
     *
     * @param title
     */
    public void setAppBarTitle(String title) {
        this.mAppBarTitle = title;
        if (mAppBarTitle != null) {
            mTvAppBarName.setText(mAppBarTitle);
        }
    }

    public void setAppBarTitleVisible(int visible) {
        if (mAppBarTitle != null) {
            mTvAppBarName.setVisibility(visible);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    /**
     * 设置标题拦颜色
     *
     * @param color
     */
    public void setAppBarColor(int color) {
        mAppBarColor = color;
        mBackBtn.setColorFilter(mAppBarColor);
        mTvAppBarName.setTextColor(mAppBarColor);
    }

    /**
     * 左边按钮点击事件
     *
     * @param mLeftBtnOnClickListener
     */
    public void setLeftBtnOnClickListener(OnClickListener mLeftBtnOnClickListener) {
        this.mLeftBtnOnClickListener = mLeftBtnOnClickListener;
    }

    public void setRightTextOnClickListener(OnClickListener onClickListener) {
        this.mRightTextOnClickListener = onClickListener;
    }

    /**
     * 设置背景
     *
     * @param res
     */
    public void setAppBarBg(@DrawableRes int res) {
        mIvBg.setImageResource(res);
    }

    /**
     * AppBar 背景透明度
     *
     * @param progress
     */
    public void setAppBarBgAlpha(float progress) {
        mIvBg.setAlpha(progress);
    }
}
