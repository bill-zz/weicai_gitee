package com.weichai.app.views.progress;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.weichai.app.R;


public class ProgressCircleView extends FrameLayout {
    public ProgressCircleView(@NonNull Context context) {
        super(context);
        init();
    }

    public ProgressCircleView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ProgressCircleView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    public void init() {
        inflate(getContext(), R.layout.view_circle_progress, this);
    }
}
