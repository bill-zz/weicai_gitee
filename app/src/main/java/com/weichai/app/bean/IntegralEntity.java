package com.weichai.app.bean;

/**
 * create by bill on 4.12.21
 */
public class IntegralEntity {

    private String name;
    private String time;
    private String money;

    public IntegralEntity() {
    }

    public IntegralEntity(String name, String time, String money) {
        this.name = name;
        this.time = time;
        this.money = money;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}
