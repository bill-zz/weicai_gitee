package com.weichai.app.bean;

/**
 * create by bill on 11.12.21
 */
public class DictRes {
    /**
     * code
     */
    private String code;
    /**
     * createPerson
     */
    private String createPerson;
    /**
     * createTime
     */
    private String createTime;
    /**
     * def1
     */
    private Object def1;
    /**
     * def2
     */
    private Object def2;
    /**
     * def3
     */
    private Object def3;
    /**
     * def4
     */
    private Object def4;
    /**
     * dictValueStatus
     */
    private Object dictValueStatus;
    /**
     * dr
     */
    private int dr;
    /**
     * extFields
     */
    private ExtFieldsDTO extFields;
    /**
     * extension
     */
    private String extension;
    /**
     * groupCode
     */
    private String groupCode;
    /**
     * id
     */
    private String id;
    /**
     * instanceId
     */
    private String instanceId;
    /**
     * statement
     */
    private String statement;
    /**
     * status
     */
    private int status;
    /**
     * tenantId
     */
    private String tenantId;
    /**
     * type
     */
    private String type;
    /**
     * updatePerson
     */
    private String updatePerson;
    /**
     * updateTime
     */
    private String updateTime;
    /**
     * value
     */
    private String value;

    private boolean select;

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(String createPerson) {
        this.createPerson = createPerson;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Object getDef1() {
        return def1;
    }

    public void setDef1(Object def1) {
        this.def1 = def1;
    }

    public Object getDef2() {
        return def2;
    }

    public void setDef2(Object def2) {
        this.def2 = def2;
    }

    public Object getDef3() {
        return def3;
    }

    public void setDef3(Object def3) {
        this.def3 = def3;
    }

    public Object getDef4() {
        return def4;
    }

    public void setDef4(Object def4) {
        this.def4 = def4;
    }

    public Object getDictValueStatus() {
        return dictValueStatus;
    }

    public void setDictValueStatus(Object dictValueStatus) {
        this.dictValueStatus = dictValueStatus;
    }

    public int getDr() {
        return dr;
    }

    public void setDr(int dr) {
        this.dr = dr;
    }

    public ExtFieldsDTO getExtFields() {
        return extFields;
    }

    public void setExtFields(ExtFieldsDTO extFields) {
        this.extFields = extFields;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(String updatePerson) {
        this.updatePerson = updatePerson;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static class ExtFieldsDTO {
    }
}
