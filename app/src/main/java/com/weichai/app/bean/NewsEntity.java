package com.weichai.app.bean;

public class NewsEntity {
    private String activeTime;
    public String content;
    private String createdAt;
    private String createdBy;
    private ExtFieldsBean extFields;
    private Integer id;
    private Integer instanceId;
    private Integer isRead;
    private Integer messageType;
    private Integer publishId;
    private String publishTime;
    private String remark;
    private Integer state;
    private Integer tenantId;
    private String title;
    private String updatedAt;
    private String updatedBy;
    private String userCode;

    public static class ExtFieldsBean {

    }
}
