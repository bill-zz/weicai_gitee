package com.weichai.app.bean;

/**
 * create by bill on 4.12.21
 */
public class EngineEntity {

    private String no;
    private String engineUrl;

    public EngineEntity() {
    }

    public EngineEntity(String no, String engineUrl) {
        this.no = no;
        this.engineUrl = engineUrl;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getEngineUrl() {
        return engineUrl;
    }

    public void setEngineUrl(String engineUrl) {
        this.engineUrl = engineUrl;
    }
}
