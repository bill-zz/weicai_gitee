package com.weichai.app.bean;

/**
 * @ClassName: BaseResult
 * @Description:
 * @Author: asanant
 * @Date: 2021/11/15 9:05 下午
 */
public class BaseResult<T> {
    public T data;
    public String resultCode;
    public String resultMsg;

}
