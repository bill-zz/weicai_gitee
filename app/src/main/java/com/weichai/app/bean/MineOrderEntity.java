package com.weichai.app.bean;

/**
 * create by bill on 6.12.21
 */
public class MineOrderEntity {

    private String workNumber;
    private String engineNumber;
    private String station;
    private String time;

    public MineOrderEntity(String workNumber, String engineNumber, String station, String time) {
        this.workNumber = workNumber;
        this.engineNumber = engineNumber;
        this.station = station;
        this.time = time;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

