package com.weichai.app.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class FaultCodeEntity implements Parcelable {
    public String ecuType;
    public String ecuVersion;
    private String failureReason;
    public String hitchCode;
    public String hitchDescribe;
    private Object instanceId;
    private Object tenantId;

    public static class ExtFieldsBean {

    }

    protected FaultCodeEntity(Parcel in) {
        ecuType = in.readString();
        ecuVersion = in.readString();
        failureReason = in.readString();
        hitchCode = in.readString();
        hitchDescribe = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ecuType);
        dest.writeString(ecuVersion);
        dest.writeString(failureReason);
        dest.writeString(hitchCode);
        dest.writeString(hitchDescribe);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FaultCodeEntity> CREATOR = new Creator<FaultCodeEntity>() {
        @Override
        public FaultCodeEntity createFromParcel(Parcel in) {
            return new FaultCodeEntity(in);
        }

        @Override
        public FaultCodeEntity[] newArray(int size) {
            return new FaultCodeEntity[size];
        }
    };
}
