package com.weichai.app.bean;

/**
 * create by bill on 9.12.21
 */
public class ChangePhoneReq {

    public String userName;
    public String verificationCode;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    @Override
    public String toString() {
        return "ChangePhoneReq{" +
                "userName='" + userName + '\'' +
                ", verificationCode='" + verificationCode + '\'' +
                '}';
    }
}
