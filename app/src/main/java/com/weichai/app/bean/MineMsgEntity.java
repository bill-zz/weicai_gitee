package com.weichai.app.bean;

/**
 * create by bill on 6.12.21
 */
public class MineMsgEntity {

    private String picUrl;
    private String title;
    private String detail;
    private String time;

    public MineMsgEntity() {
    }

    public MineMsgEntity(String picUrl, String title, String detail, String time) {
        this.picUrl = picUrl;
        this.title = title;
        this.detail = detail;
        this.time = time;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
