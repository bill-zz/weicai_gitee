package com.weichai.app.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class ServiceStationEntity implements Parcelable {

    /**
     * 区名
     */
    private String areaName;

    /**
     * 维修站详细地址
     */
    public String detailedAddress;

    /**
     * 纬度
     */
    public String dimension;
    private ExtFieldsBean extFields;
    private Integer instanceId;

    /**
     * 经度
     */
    public String longitude;

    /**
     * 市名
     */
    private String marketName;

    /**
     * 省名
     */
    private String provinceName;

    /**
     * 服务站编号
     */
    private String serviceCode;

    /**
     * 服务站名称
     */
    public String serviceStation;

    /**
     * 维修站电话
     */
    public String sitePhone;
    private Integer tenantId;

    public static class ExtFieldsBean {

    }

    protected ServiceStationEntity(Parcel in) {
        areaName = in.readString();
        detailedAddress = in.readString();
        dimension = in.readString();
        if (in.readByte() == 0) {
            instanceId = null;
        } else {
            instanceId = in.readInt();
        }
        longitude = in.readString();
        marketName = in.readString();
        provinceName = in.readString();
        serviceCode = in.readString();
        serviceStation = in.readString();
        sitePhone = in.readString();
        if (in.readByte() == 0) {
            tenantId = null;
        } else {
            tenantId = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(areaName);
        dest.writeString(detailedAddress);
        dest.writeString(dimension);
        if (instanceId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(instanceId);
        }
        dest.writeString(longitude);
        dest.writeString(marketName);
        dest.writeString(provinceName);
        dest.writeString(serviceCode);
        dest.writeString(serviceStation);
        dest.writeString(sitePhone);
        if (tenantId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(tenantId);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ServiceStationEntity> CREATOR = new Creator<ServiceStationEntity>() {
        @Override
        public ServiceStationEntity createFromParcel(Parcel in) {
            return new ServiceStationEntity(in);
        }

        @Override
        public ServiceStationEntity[] newArray(int size) {
            return new ServiceStationEntity[size];
        }
    };
}
