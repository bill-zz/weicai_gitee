package com.weichai.app.bean;

import android.util.Log;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.weichai.app.BR;


/**
 * create by bill on 4.12.21
 */
public class SelectItemEntity extends BaseObservable {

    private String itemMsg;

    @Bindable
    private boolean select;

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
        Log.i("weicai", "setSelect: " + itemMsg + " isSelect " + select);
        notifyChange();
    }


    public SelectItemEntity(String itemMsg) {
        this.itemMsg = itemMsg;
    }

    public String getItemMsg() {
        return itemMsg;
    }

    public void setItemMsg(String itemMsg) {
        this.itemMsg = itemMsg;
    }
}
