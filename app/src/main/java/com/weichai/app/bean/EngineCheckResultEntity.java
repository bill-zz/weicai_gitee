package com.weichai.app.bean;

/**
 * create by bill on 4.12.21
 */
public class EngineCheckResultEntity {

    private String title;
    private String time;

    public EngineCheckResultEntity(String title, String time) {
        this.title = title;
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
