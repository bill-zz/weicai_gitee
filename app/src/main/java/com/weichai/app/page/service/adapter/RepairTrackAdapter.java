package com.weichai.app.page.service.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.bean.EngineEntity;
import com.weichai.app.bean.RepairTrackEntity;


public class RepairTrackAdapter extends BaseQuickAdapter<RepairTrackEntity, BaseViewHolder> {

    public RepairTrackAdapter() {
        super(R.layout.item_repair_tracking);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, RepairTrackEntity repairTrackEntity) {

        baseViewHolder.setText(R.id.tv_order_no_value, repairTrackEntity.getNo());
        baseViewHolder.setText(R.id.tv_order_station_value, repairTrackEntity.getStation());
        baseViewHolder.setText(R.id.tv_order_time_value, repairTrackEntity.getTime());

    }
}
