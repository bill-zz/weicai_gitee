package com.weichai.app.page.mine.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.bean.MineCouponsEntity;
import com.weichai.app.bean.MineOrderEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * create by bill on 7.12.21
 */
public class MineCouponsVM extends BaseViewModel {

    public MutableLiveData data = new MutableLiveData<List<MineCouponsEntity>>();

    public void fetchData() {
        test();
    }

    private void test() {
        ArrayList<MineCouponsEntity> entities = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            entities.add(new MineCouponsEntity("SP2111170001-" + i));
        }
        data.setValue(entities);
    }
}
