package com.weichai.app.page.mine.view;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentMineIntegralBinding;
import com.weichai.app.page.mine.viewmodel.MineIntegralVM;

import java.util.ArrayList;

/**
 * create by bill on 4.12.21
 */
public class MineIntegralFragment extends BaseFragment<FragmentMineIntegralBinding, MineIntegralVM> {
    @Override
    protected void initView() {
        TabLayout tab = mViewBinding.tab;

        ArrayList<String> tabName = new ArrayList<>();
        tabName.add(requireContext().getString(R.string.integral_all));
        tabName.add(requireContext().getString(R.string.integral_get));
        tabName.add(requireContext().getString(R.string.integral_consume));

        tab.addTab(tab.newTab().setText(tabName.get(0)));
        tab.addTab(tab.newTab().setText(tabName.get(1)));
        tab.addTab(tab.newTab().setText(tabName.get(2)));



        MineIntegralChildFragment all = new MineIntegralChildFragment();
        MineIntegralChildFragment get = new MineIntegralChildFragment();
        MineIntegralChildFragment consume = new MineIntegralChildFragment();

        ArrayList<Fragment> list = new ArrayList<>();
        list.add(all);
        list.add(get);
        list.add(consume);

        mViewBinding.vp.setOffscreenPageLimit(ViewPager2.OFFSCREEN_PAGE_LIMIT_DEFAULT);
        mViewBinding.vp.setAdapter(new FragmentStateAdapter(getChildFragmentManager(), getLifecycle()) {
            @NonNull
            @Override
            public Fragment createFragment(int position) {
                return list.get(position);
            }

            @Override
            public int getItemCount() {
                return list.size();
            }
        });

        new TabLayoutMediator(
                tab,
                mViewBinding.vp,
                (tab1, position) -> {
                    tab1.setText(tabName.get(position));
                }
        ).attach();
    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine_integral;
    }
}
