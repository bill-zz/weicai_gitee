package com.weichai.app.page.mine.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.bean.MineMsgEntity;
import com.weichai.app.bean.MineOrderEntity;
import com.weichai.app.databinding.ItemMineIntegralBinding;
import com.weichai.app.databinding.ItemMineOrderBinding;

/**
 * create by bill on 6.12.21
 */
public class OrderAdapter extends BaseQuickAdapter<MineOrderEntity, BaseDataBindingHolder<ItemMineOrderBinding>> {

    public OrderAdapter() {
        super(R.layout.item_mine_order);
    }


    @Override
    protected void convert(@NonNull BaseDataBindingHolder<ItemMineOrderBinding> holder, MineOrderEntity orderEntity) {
        ItemMineOrderBinding dataBinding = holder.getDataBinding();
        if (dataBinding != null) {
            dataBinding.tvWorkNumberValue.setText( orderEntity.getWorkNumber());
            dataBinding.tvEngineNoValue.setText( orderEntity.getEngineNumber());
            dataBinding.tvStationValue.setText( orderEntity.getStation());
            dataBinding.tvTimeValue.setText( orderEntity.getTime());
        }
    }
}
