package com.weichai.app.page.mine.view;

import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.bean.MineMsgEntity;
import com.weichai.app.bean.MineOrderEntity;
import com.weichai.app.databinding.FragmentMineChildOrderBinding;
import com.weichai.app.page.mine.adapter.FaultMsgAdapter;
import com.weichai.app.page.mine.adapter.ItemDecoration;
import com.weichai.app.page.mine.adapter.OrderAdapter;
import com.weichai.app.page.mine.viewmodel.MineOrderVM;

import java.util.List;

/**
 * create by bill on 6.12.21
 */
public class MineOrderChildFragment extends BaseFragment<FragmentMineChildOrderBinding, MineOrderVM> {

    private final OrderAdapter adapter=new OrderAdapter();


    @Override
    protected void initView() {
        int decoration = requireContext().getResources().getDimensionPixelSize(R.dimen.dp_15);
        mViewBinding.rv.addItemDecoration(new ItemDecoration(0, 0, 0, decoration));
        mViewBinding.rv.setLayoutManager(new LinearLayoutManager(requireContext()));
        mViewBinding.rv.setAdapter(adapter);
    }

    @Override
    protected void initData() {
        mViewModel.fetchData();
        mViewModel.data.observe(this, new Observer<List<MineOrderEntity>>() {
            @Override
            public void onChanged(List<MineOrderEntity> list) {
                adapter.setList(list);
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine_child_order;
    }
}
