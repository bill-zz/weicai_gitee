package com.weichai.app.page.service.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.bean.EngineCheckResultEntity;


public class EngineCheckResultAdapter extends BaseQuickAdapter<EngineCheckResultEntity, BaseViewHolder> {

    public EngineCheckResultAdapter() {
        super(R.layout.item_engine_check_result);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, EngineCheckResultEntity checkResultEntity) {

        baseViewHolder.setText(R.id.tv_title, checkResultEntity.getTitle());
        baseViewHolder.setText(R.id.tv_time, checkResultEntity.getTime());

    }
}
