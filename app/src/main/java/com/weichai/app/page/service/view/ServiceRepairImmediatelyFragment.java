package com.weichai.app.page.service.view;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FramgentRepairImmediatelyBinding;
import com.weichai.app.page.service.viewmodel.RepairVM;


public class ServiceRepairImmediatelyFragment extends BaseFragment<FramgentRepairImmediatelyBinding, RepairVM> {
    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.framgent_repair_immediately;
    }
}
