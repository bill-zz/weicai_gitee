package com.weichai.app.page.login.viewmodel;

import static com.weichai.app.page.common.PrivacyFragment.KEY_PRIVACY_TYPE;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.databinding.ObservableField;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.arch.http.ApiManager;
import com.weichai.app.arch.http.ApiSubscriber;
import com.weichai.app.arch.http.ApiTransformer;
import com.weichai.app.bean.BaseResult;
import com.weichai.app.bean.UserInfo;
import com.weichai.app.page.mine.UserCenterVM;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import dev.utils.common.validator.ValiToPhoneUtils;
import io.reactivex.rxjava3.core.Observable;

public class BindPhoneVM extends BaseViewModel {
    public ObservableField<String> textPhone = new ObservableField<>();//手机号
    public ObservableField<String> textAuthCode = new ObservableField<>();//验证码
    public ObservableField<String> textAuthBtn = new ObservableField<>("获取验证码");//获取验证按钮
    public ObservableField<Boolean> enableAuthBtn = new ObservableField<>(true);//是否可用获取验证码按钮
    public ObservableField<Boolean> enableCommitBtn = new ObservableField<>(false);//是否可用提交按钮
    public ObservableField<Boolean> checkPrivacy = new ObservableField<>(true);//
    public ObservableField<Boolean> canBindPhone = new ObservableField<>(true);//
    public ObservableField<Integer> phoneErrVisible = new ObservableField<>(View.GONE);//
    public String wxOpenId = "";
    public String wxAvatar = "";
    public String wxNickName = "";
    public String wxUserCode = "";
    public String wxUnionId = "";

    /**
     * 提交用户注册
     */
    public void commit() {
        if (!checkPrivacy.get()) {
            showToast("请同意用户隐私协议");
            return;
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("userName", textPhone.get());
        params.put("verificationCode", textAuthCode.get());
        params.put("openId", wxOpenId);
        params.put("header", wxAvatar);
        params.put("nickName", wxNickName);
        params.put("userCode", wxUserCode);
        params.put("unionId", wxUnionId);

        ApiManager
                .getInstance()
                .bindingPhone(params)
                .compose(new ApiTransformer<>())
                .subscribeWith(new ApiSubscriber<UserInfo>(true, true, this) {
                    @Override
                    public void onSuccess(BaseResult<UserInfo> result) {
                        navigate(R.id.bind_to_main_page);
                        getViewModelOfApp(UserCenterVM.class).updateUserInfo(result.data);
                        showToast("绑定成功");
                    }
                });
    }


    /**
     * 获取验证码
     */
    public void getPhoneCode() {
        if (TextUtils.isEmpty(textPhone.get())) {
            showToast("请填写手机号");
            return;
        }
        if (!canBindPhone.get()) {
            showToast("该手机号已被绑定");
            return;
        }
        ApiManager
                .getInstance()
                .sendVerify(textPhone.get(), "4")
                .compose(new ApiTransformer<>())
                .subscribeWith(new ApiSubscriber<String>(true, true, this) {
                    @Override
                    public void onSuccess(BaseResult<String> result) {
                        showToast("发送成功");
                        intervalAuthCode();
                    }
                });
    }



    /**
     * 验证码倒计时
     */
    private void intervalAuthCode() {
        Observable
                .interval(1, 1, TimeUnit.SECONDS)
                .take(61)
                .subscribe(value -> {
                    int time = (int) (60 - value);
                    if (time == 0) {
                        textAuthBtn.set(String.format("重新获取验证码", time));
                        enableAuthBtn.set(true);
                    } else {
                        textAuthBtn.set(String.format("%sS后重新获取", time));
                        enableAuthBtn.set(false);
                    }
                });
    }


    //文字变化监听
    public void onTextChanged(CharSequence value, int i, int i1, int i2) {
        ///延迟获取最新输入值
        Observable.just(1)
                .delay(50, TimeUnit.MILLISECONDS)
                .subscribe(integer -> {
                    if (!TextUtils.isEmpty(textPhone.get()) && !ValiToPhoneUtils.isPhoneCheck(textPhone.get())) {
                        phoneErrVisible.set(View.VISIBLE);
                        canBindPhone.set(true);
                    } else {
                        phoneErrVisible.set(View.GONE);
                        if (!TextUtils.isEmpty(textPhone.get())) {
                            ApiManager.getInstance().bindingVerifyPhone(textPhone.get())
                                    .compose(new ApiTransformer<>())
                                    .subscribeWith(new ApiSubscriber<Boolean>() {
                                        @Override
                                        public void onSuccess(BaseResult<Boolean> result) {
                                            canBindPhone.set(result.data);
                                        }
                                    });
                        }
                    }
                    enableCommitBtn.set(checkCommit());
                });
    }


    /**
     * 检查是否可以提交注册
     *
     * @return 是/否
     */
    public boolean checkCommit() {

        if (!ValiToPhoneUtils.isPhoneCheck(textPhone.get())) {
            return false;
        }
        if (!canBindPhone.get()) {
            return false;
        }
        if (TextUtils.isEmpty(textAuthCode.get())) {
            return false;
        }

        return true;
    }

    public void navPrivacyPage(int type) {
        Bundle param = new Bundle();
        param.putString(KEY_PRIVACY_TYPE, String.valueOf(type));
        navigate(R.id.privacy_page, param);
    }
}