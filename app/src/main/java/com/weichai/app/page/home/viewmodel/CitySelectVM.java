package com.weichai.app.page.home.viewmodel;

import com.kunminx.architecture.ui.callback.UnPeekLiveData;
import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.arch.http.ApiManager;
import com.weichai.app.arch.http.ApiSubscriber;
import com.weichai.app.arch.http.ApiTransformer;
import com.weichai.app.bean.BaseResult;
import com.weichai.app.bean.CityEntity;

import java.util.HashMap;
import java.util.List;

public class CitySelectVM extends BaseViewModel {
    public UnPeekLiveData<List<CityEntity>> cityEntityList = new UnPeekLiveData<>();

    public void queryMarket() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("pageNum", PAGE_NUM);
        params.put("pageSize", PAGE_SIZE);

        ApiManager.getInstance()
                .queryMarket(params)
                .compose(new ApiTransformer<>())
                .subscribeWith(new ApiSubscriber<List<CityEntity>>(true, false, this) {
                    @Override
                    public void onSuccess(BaseResult<List<CityEntity>> result) {
                        cityEntityList.setValue(result.data);
                    }
                });
    }
}
