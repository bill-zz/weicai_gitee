package com.weichai.app.page.service.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.bean.EngineEntity;


public class EngineListAdapter extends BaseQuickAdapter<EngineEntity, BaseViewHolder> {

    public EngineListAdapter() {
        super(R.layout.item_service_engine);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, EngineEntity engineEntity) {

        baseViewHolder.setText(R.id.tv_engine_no, engineEntity.getNo());

    }
}
