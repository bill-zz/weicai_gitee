package com.weichai.app.page.login.view;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentRegisterBinding;
import com.weichai.app.page.login.viewmodel.RegisterVM;

public class RegisterFragment extends BaseFragment<FragmentRegisterBinding, RegisterVM> {


    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_register;
    }
}