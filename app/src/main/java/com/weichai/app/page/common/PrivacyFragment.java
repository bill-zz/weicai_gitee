package com.weichai.app.page.common;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentPrivacyBinding;

/**
 * @ClassName: PrivacyFragment
 * @Description: 隐私协议页面
 * @Author: 祖安
 * @Date: 2021/11/26 7:11 下午
 */
public class PrivacyFragment extends BaseFragment<FragmentPrivacyBinding, PrivacyVM> {
    public static final String KEY_PRIVACY_TYPE = "key_privacy_type";
    private String type = "1";

    @Override
    protected void initView() {
        mViewBinding.appBar.setAppBarTitle(type.equals("1") ? "用户协议" : "隐私政策");

    }

    @Override
    protected void initData() {
        type = getArguments().getString(KEY_PRIVACY_TYPE, "1");
        mViewModel.getProtocol(type);

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_privacy;
    }
}
