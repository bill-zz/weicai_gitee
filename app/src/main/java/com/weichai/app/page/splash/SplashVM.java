package com.weichai.app.page.splash;

import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import androidx.databinding.ObservableField;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.arch.config.CacheConstant;
import com.weichai.app.arch.http.ApiException;
import com.weichai.app.arch.http.ApiManager;
import com.weichai.app.arch.http.ApiServer;
import com.weichai.app.arch.http.ApiSubscriber;
import com.weichai.app.arch.http.ApiTransformer;
import com.weichai.app.arch.utils.MMKVUtil;
import com.weichai.app.arch.utils.UserInfoManager;
import com.weichai.app.bean.BaseResult;
import com.weichai.app.bean.UserInfo;
import com.weichai.app.page.mine.UserCenterVM;

import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;

/**
 * @ClassName: SplashVM
 * @Description:
 * @Author: asanant
 * @Date: 2021/11/27 9:49 上午
 */
public class SplashVM extends BaseViewModel {
    public ObservableField<String> skinText = new ObservableField<>("跳过");
    private Disposable mDisposable;

    /**
     * 跳过倒计时
     */
    void startSkinCountDown() {
        mDisposable = Observable
                .interval(1, TimeUnit.SECONDS)
                .take(1)
                .subscribe(value -> {
                    int time = (int) (1 - value);
                    if (value == 0) {
                        navigateMainPage();
                    }
                    skinText.set(String.format("跳过%s", time));
                });
    }

    /**
     * 进入主页/登录页
     */
    public void navigateMainPage() {
        if (mDisposable != null) {
            mDisposable.dispose();
        }

        String userToken = MMKVUtil.getString(CacheConstant.KEY_USER_TOKEN);
        String userInfo = MMKVUtil.getString(CacheConstant.KEY_USER_INFO);
        Log.i("weicai", "userJson: " + userToken);
        Log.i("weicai", "userInfo: " + userInfo);

        if (userToken == null || userToken.equals("")) {
            navigate(R.id.splash_to_login_page);
        }else {
            navigate(R.id.splash_to_main_page);
        }

//        ApiManager.getInstance()
//                .getUserInfo()
//                .compose(new ApiTransformer<>())
//                .subscribeWith(new ApiSubscriber<UserInfo>(true, false, this) {
//                    @Override
//                    public void onSuccess(BaseResult<UserInfo> result) {
//                        getViewModelOfApp(UserCenterVM.class).updateUserInfo(result.data);
//                        UserInfoManager.getInstance().setUserInfo(result.data);
//                        Log.i("weicai", "userInfo: " + result.data);
//                        navigate(R.id.splash_to_main_page);
//                    }
//
//                    @Override
//                    public void onFail(ApiException apiException) {
//                        super.onFail(apiException);
//                        navigate(R.id.splash_to_login_page);
//
//                    }
//                });
    }

}
