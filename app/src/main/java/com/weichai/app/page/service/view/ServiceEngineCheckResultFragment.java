package com.weichai.app.page.service.view;

import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.bean.EngineCheckResultEntity;
import com.weichai.app.bean.EngineEntity;
import com.weichai.app.databinding.FragmentEngineCheckResultBinding;
import com.weichai.app.databinding.FragmentServiceEngineListBinding;
import com.weichai.app.page.mine.adapter.ItemDecoration;
import com.weichai.app.page.service.adapter.EngineCheckResultAdapter;
import com.weichai.app.page.service.adapter.EngineListAdapter;
import com.weichai.app.page.service.viewmodel.EngineCheckResultVM;
import com.weichai.app.page.service.viewmodel.EngineListVM;

import java.util.List;


public class ServiceEngineCheckResultFragment extends BaseFragment<FragmentEngineCheckResultBinding, EngineCheckResultVM> {

    private final EngineCheckResultAdapter adapter = new EngineCheckResultAdapter();


    @Override
    protected void initView() {
        mViewBinding.rv.setLayoutManager(new LinearLayoutManager(requireContext()));
        mViewBinding.rv.setAdapter(adapter);

    }

    @Override
    protected void initData() {
        mViewModel.fetchEngineCheckoutData();
        mViewModel.engineCheckoutData.observe(this, new Observer<List<EngineCheckResultEntity>>() {
            @Override
            public void onChanged(List<EngineCheckResultEntity> data) {
                adapter.setList(data);
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_engine_check_result;
    }
}
