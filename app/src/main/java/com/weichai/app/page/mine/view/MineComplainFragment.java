package com.weichai.app.page.mine.view;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentMineComplainBinding;
import com.weichai.app.page.mine.viewmodel.MineComplainVM;

/**
 * 用户信息
 *
 */
public class MineComplainFragment extends BaseFragment<FragmentMineComplainBinding, MineComplainVM> {

    @Override
    protected void initView() {
    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine_complain;
    }
}
