package com.weichai.app.page.mine.view;

import android.util.Log;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentMineComplainBinding;
import com.weichai.app.databinding.FragmentMineSettingBinding;
import com.weichai.app.page.mine.viewmodel.MineComplainVM;
import com.weichai.app.page.mine.viewmodel.MineSettingVM;

/**
 * 用户信息
 *
 */
public class MineSettingFragment extends BaseFragment<FragmentMineSettingBinding, MineSettingVM> {

    @Override
    protected void initView() {
        Log.i("weicai", "initView: MineSettingFragment");
    }

    @Override
    protected void initData() {
        Log.i("weicai", "initData: MineSettingFragment");
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine_setting;
    }
}
