package com.weichai.app.page.home.viewmodel;

import com.kunminx.architecture.ui.callback.UnPeekLiveData;
import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.arch.http.ApiManager;
import com.weichai.app.arch.http.ApiSubscriber;
import com.weichai.app.arch.http.ApiTransformer;
import com.weichai.app.bean.BaseResult;
import com.weichai.app.bean.ServiceStationEntity;

import java.util.HashMap;
import java.util.List;

public class ServiceStationVM extends BaseViewModel {
    public UnPeekLiveData<List<ServiceStationEntity>> serviceStationList = new UnPeekLiveData<>();
    public UnPeekLiveData<String> searchContent = new UnPeekLiveData<>();
    public UnPeekLiveData<String> dimension = new UnPeekLiveData<>();
    public UnPeekLiveData<String> longitude = new UnPeekLiveData<>();

    public void search() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("dimension", dimension.getValue());
        params.put("longitude", longitude.getValue());
        params.put("pageNum", PAGE_NUM);
        params.put("pageSize", PAGE_SIZE);
        params.put("serviceStation", searchContent.getValue());

        ApiManager.getInstance()
                .queryBySite(params)
                .compose(new ApiTransformer<>())
                .subscribeWith(new ApiSubscriber<List<ServiceStationEntity>>(true, false, this) {
                    @Override
                    public void onSuccess(BaseResult<List<ServiceStationEntity>> result) {
                        serviceStationList.setValue(result.data);
                    }
                });
    }
}
