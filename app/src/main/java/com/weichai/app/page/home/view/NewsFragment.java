package com.weichai.app.page.home.view;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentNewsBinding;
import com.weichai.app.page.home.viewmodel.NewsVM;

/**
 * 消息fragment
 */
public class NewsFragment extends BaseFragment<FragmentNewsBinding, NewsVM> {

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_news;
    }
}
