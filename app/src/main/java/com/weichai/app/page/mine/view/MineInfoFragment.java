package com.weichai.app.page.mine.view;

import com.weichai.app.BR;
import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentMineInfoBinding;
import com.weichai.app.page.mine.UserCenterVM;
import com.weichai.app.page.mine.viewmodel.MineInfoVM;

/**
 * 用户信息
 *
 */
public class MineInfoFragment extends BaseFragment<FragmentMineInfoBinding, MineInfoVM> {

    @Override
    protected void initView() {
        ///绑定全局用户信息数据
        mViewBinding.setVariable(BR.userVM, getViewModelOfApp(UserCenterVM.class));
    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine_info;
    }
}
