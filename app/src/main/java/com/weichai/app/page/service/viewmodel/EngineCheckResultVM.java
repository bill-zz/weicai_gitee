package com.weichai.app.page.service.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.bean.EngineCheckResultEntity;
import com.weichai.app.bean.EngineEntity;

import java.util.ArrayList;
import java.util.List;


public class EngineCheckResultVM extends BaseViewModel {

    public MutableLiveData<List<EngineCheckResultEntity>> engineCheckoutData = new MutableLiveData();


    public void fetchEngineCheckoutData() {
        test();
    }

    public void test() {
        List<EngineCheckResultEntity> engineList = new ArrayList();
        for (int i = 0; i < 10; i++) {
            engineList.add(new EngineCheckResultEntity("故障提醒"+i, "2021-12-07 17:00"));
        }
        engineCheckoutData.postValue(engineList);
    }
}
