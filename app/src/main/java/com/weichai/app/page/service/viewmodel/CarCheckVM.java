package com.weichai.app.page.service.viewmodel;

import com.kunminx.architecture.ui.callback.UnPeekLiveData;
import com.weichai.app.arch.base.BaseViewModel;

public class CarCheckVM extends BaseViewModel {
    public UnPeekLiveData<Boolean> isStart = new UnPeekLiveData<>();

    public void init() {
        isStart.setValue(true);
    }

    /**
     * 开始检测
     */
    public void startCheck() {
        isStart.setValue(false);
    }

    /**
     * 暂停检测
     */
    public void stopCheck() {
        isStart.setValue(true);
    }
}
