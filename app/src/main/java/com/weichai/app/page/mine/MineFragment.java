package com.weichai.app.page.mine;

import android.util.Log;

import com.weichai.app.BR;
import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.arch.utils.UserInfoManager;
import com.weichai.app.bean.UserInfo;
import com.weichai.app.databinding.FragmentMineBinding;

/**
 * @ClassName: MineFragment
 * @Description: 我的界面
 * @Author: 祖安
 * @Date: 2021/9/1 5:06 下午
 */
public class MineFragment extends BaseFragment<FragmentMineBinding, MineVM> {
    @Override
    protected void initView() {
        ///绑定全局用户信息数据
        mViewBinding.setVariable(BR.userVM, getViewModelOfApp(UserCenterVM.class));
    }

    @Override
    protected void initData() {
        Log.i("weicai", "initData: MineFragment");

        UserInfo userInfo = UserInfoManager.getInstance().getUserInfo();
        Log.i("weicai", "initData: "+userInfo);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.i("weicai", "onDestroy: MineFragment");

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine;
    }
}
