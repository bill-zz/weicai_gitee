package com.weichai.app.page.mine.view;

import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.google.android.material.tabs.TabLayoutMediator;
import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentMineMsgBinding;
import com.weichai.app.page.mine.viewmodel.MineMsgVM;

import java.util.ArrayList;
import java.util.List;

import dev.utils.app.BarUtils;

/**
 * create by bill on 6.12.21
 */
public class MineMsgFragment extends BaseFragment<FragmentMineMsgBinding, MineMsgVM> {
    @Override
    protected void initView() {

        List<String> tabList = new ArrayList<>();
        tabList.add("故障信息");
        tabList.add("获得信息");
        tabList.add("系统信息");

        mViewBinding.tab.addTab(mViewBinding.tab.newTab().setText(tabList.get(0)));
        mViewBinding.tab.addTab(mViewBinding.tab.newTab().setText(tabList.get(1)));
        mViewBinding.tab.addTab(mViewBinding.tab.newTab().setText(tabList.get(2)));

        MineMsgFaulFragment mineMsgFaulFragment = new MineMsgFaulFragment();
        MineMsgAcFragment mineMsgAcFragment = new MineMsgAcFragment();
        MineMsgSysFragment mineMsgSysFragment = new MineMsgSysFragment();

        List<Fragment> fgList = new ArrayList<>();
        fgList.add(mineMsgFaulFragment);
        fgList.add(mineMsgAcFragment);
        fgList.add(mineMsgSysFragment);

        mViewBinding.vp.setAdapter(new FragmentStateAdapter(getChildFragmentManager(), getLifecycle()) {
            @NonNull
            @Override
            public Fragment createFragment(int position) {
                return fgList.get(position);
            }

            @Override
            public int getItemCount() {
                return fgList.size();
            }
        });

        new TabLayoutMediator(
                mViewBinding.tab,
                mViewBinding.vp,
                (tab1, position) -> {
                    tab1.setText(tabList.get(position));
                }
        ).attach();

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine_msg;
    }
}
