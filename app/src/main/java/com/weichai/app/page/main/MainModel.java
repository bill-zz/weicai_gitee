package com.weichai.app.page.main;

import com.baidu.mapapi.model.LatLng;
import com.kunminx.architecture.ui.callback.UnPeekLiveData;
import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.bean.EntranceEntity;
import com.weichai.app.bean.InformationEntity;

import java.util.List;

public class MainModel extends BaseViewModel {

    /**
     * 选择的城市
     */
    public UnPeekLiveData<String> selectCity = new UnPeekLiveData<>();

    /**
     * 当前定位城市
     */
    public UnPeekLiveData<String> locationCity = new UnPeekLiveData<>();

    /**
     * 当前定位经纬度
     */
    public UnPeekLiveData<LatLng> locationLatLng = new UnPeekLiveData<>();
    public UnPeekLiveData<Integer> currentItem = new UnPeekLiveData<>();
    public UnPeekLiveData<List<EntranceEntity>> entranceList = new UnPeekLiveData<>();
    public UnPeekLiveData<Boolean> recordAudioPermission = new UnPeekLiveData<>();
    public UnPeekLiveData<Boolean> locationPermission = new UnPeekLiveData<>();
    public UnPeekLiveData<Boolean> clickLike = new UnPeekLiveData<>();
}
