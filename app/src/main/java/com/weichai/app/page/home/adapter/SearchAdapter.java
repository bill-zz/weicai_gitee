package com.weichai.app.page.home.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.chad.library.adapter.base.BaseDelegateMultiAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.delegate.BaseMultiTypeDelegate;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.databinding.ItemSearchResultBinding;
import com.weichai.app.bean.SearchEntity;
import com.weichai.app.bean.SearchEntranceEntity;
import com.weichai.app.bean.SearchInfoEntity;

import java.util.List;

public class SearchAdapter extends BaseDelegateMultiAdapter<SearchEntity, BaseViewHolder> {
    public static final int ENTRANCE = 1;
    public static final int INFO = 2;
    private OnInfoItemClickListener onInfoItemClickListener;
    private OnEntranceItemClickListener onEntranceItemClickListener;

    public SearchAdapter() {
        setMultiTypeDelegate(new BaseMultiTypeDelegate<SearchEntity>() {
            @Override
            public int getItemType(@NonNull List<? extends SearchEntity> list, int position) {
                return list.get(position).type;
            }
        });
        getMultiTypeDelegate().addItemType(ENTRANCE, R.layout.item_search_result)
                .addItemType(INFO, R.layout.item_search_result);
    }

    @Override
    protected void onItemViewHolderCreated(@NonNull BaseViewHolder viewHolder, int viewType) {
        DataBindingUtil.bind(viewHolder.itemView);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, SearchEntity searchEntity) {
        if (null == searchEntity) {
            return;
        }
        ItemSearchResultBinding searchResultBinding = baseViewHolder.getBinding();
        switch (baseViewHolder.getItemViewType()) {
            case ENTRANCE:
                if (null != searchResultBinding) {
                    SearchEntranceAdapter searchEntranceAdapter = new SearchEntranceAdapter();
                    searchResultBinding.rv.setLayoutManager(new LinearLayoutManager(getContext()));
                    searchResultBinding.rv.setAdapter(searchEntranceAdapter);
                    searchEntranceAdapter.setList(searchEntity.applyDtos);
                    searchEntranceAdapter.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
                            SearchEntranceEntity searchEntranceEntity = (SearchEntranceEntity) adapter.getData().get(position);
                            if (null != onEntranceItemClickListener) {
                                onEntranceItemClickListener.onEntranceItemClick(searchEntranceEntity);
                            }
                        }
                    });
                }
                break;
            case INFO:
                if (null != searchResultBinding) {
                    SearchInfoAdapter searchInfoAdapter = new SearchInfoAdapter();
                    searchResultBinding.rv.setLayoutManager(new LinearLayoutManager(getContext()));
                    searchResultBinding.rv.setAdapter(searchInfoAdapter);
                    searchInfoAdapter.setList(searchEntity.informationDtos);
                    searchInfoAdapter.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
                            SearchInfoEntity searchInfoEntity = (SearchInfoEntity) adapter.getData().get(position);
                            if (null != onInfoItemClickListener) {
                                onInfoItemClickListener.onInfoItemClick(searchInfoEntity);
                            }
                        }
                    });
                }
                break;
            default:
                break;
        }
    }

    public interface OnInfoItemClickListener {
        void onInfoItemClick(SearchInfoEntity searchInfoEntity);
    }

    public void setOnInfoItemClickListener(OnInfoItemClickListener onInfoItemClickListener) {
        this.onInfoItemClickListener = onInfoItemClickListener;
    }

    public interface OnEntranceItemClickListener {
        void onEntranceItemClick(SearchEntranceEntity searchEntranceEntity);
    }

    public void setOnEntranceItemClickListener(OnEntranceItemClickListener onEntranceItemClickListener) {
        this.onEntranceItemClickListener = onEntranceItemClickListener;
    }
}
