package com.weichai.app.page.mine;

import static com.weichai.app.arch.config.CacheConstant.KEY_USER_INFO;

import androidx.databinding.ObservableField;

import com.kunminx.architecture.ui.callback.UnPeekLiveData;
import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.arch.config.CacheConstant;
import com.weichai.app.arch.utils.MMKVUtil;
import com.weichai.app.bean.UserInfo;

/**
 * @ClassName: UserCenterVM
 * @Description: 用户信息中心（全局使用）
 * @Author: asanant
 * @Date: 2021/11/18 10:34 下午
 */
public class UserCenterVM extends BaseViewModel {
    public ObservableField<UserInfo> userInfo = new ObservableField<>();//用户信息
    public UnPeekLiveData<String> wxLoginCodeData = new UnPeekLiveData<>();//微信登录
    public UnPeekLiveData<String> loginOutData = new UnPeekLiveData<>();//退出监听

    public UserCenterVM() {
        UserInfo user = MMKVUtil.getParcelable(KEY_USER_INFO, UserInfo.class);
        if (user != null) {
            userInfo.set(user);
        }
    }

    /**
     * 更新用户信息
     *
     * @param userInfo
     */
    public void updateUserInfo(UserInfo userInfo) {
        this.userInfo.set(userInfo);
        MMKVUtil.instance().remove(CacheConstant.KEY_LOGIN_OUT_STATE);
        MMKVUtil.save(CacheConstant.KEY_USER_TOKEN, userInfo.token);
        MMKVUtil.save(KEY_USER_INFO, userInfo);

    }

    /**
     * 退出用户登录
     *
     * @param state :1：正常退出 2：（401退出
     */
    public void loginOut(String state) {
        loginOutData.postValue(state);
        MMKVUtil.instance().putString(CacheConstant.KEY_LOGIN_OUT_STATE, state);
        MMKVUtil.instance().remove(CacheConstant.KEY_USER_TOKEN);
        if (state.equals("2")) {
            clearUserInfo();
        }

    }


    /**
     * 清楚用户信息
     */
    public void clearUserInfo() {
        this.userInfo.set(null);
        MMKVUtil.instance().remove(KEY_USER_INFO);
    }
}
