package com.weichai.app.page.login.view;

import android.os.Bundle;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentVerifyCodeLoginBinding;
import com.weichai.app.databinding.FragmentWxPhoneBindBinding;
import com.weichai.app.page.login.viewmodel.BindPhoneVM;
import com.weichai.app.page.login.viewmodel.VerifyCodeVM;

/**
 * @ClassName: VerifyCodeFragment
 * @Description: 绑定手机号
 * @Author: 祖安
 * @Date: 2021/11/19 9:45 上午
 */
public class BindPhoneFragment extends BaseFragment<FragmentWxPhoneBindBinding, BindPhoneVM> {
    public static final String WX_OPENID = "wx_openid_key";
    public static final String WX_NICK = "wx_nick_key";
    public static final String WX_UNION = "wx_union_key";
    public static final String WX_USER_CODE = "wx_user_code_key";
    public static final String WX_AVATAR = "wx_avatar_key";

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

        //页面参数赋值
        Bundle bundle = getArguments();
        if (bundle != null) {
            mViewModel.wxOpenId = bundle.getString(WX_OPENID, "");
            mViewModel.wxNickName = bundle.getString(WX_NICK, "");
            mViewModel.wxUnionId = bundle.getString(WX_UNION, "");
            mViewModel.wxUserCode = bundle.getString(WX_USER_CODE, "");
            mViewModel.wxAvatar = bundle.getString(WX_AVATAR, "");
        }

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_wx_phone_bind;
    }
}
