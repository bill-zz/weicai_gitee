package com.weichai.app.page.home.view;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.TextureMapView;
import com.baidu.mapapi.model.LatLng;
import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.arch.utils.GpsUtil;
import com.weichai.app.bean.ServiceStationEntity;
import com.weichai.app.databinding.FragmentStationDetailsBinding;
import com.weichai.app.page.home.dialog.CancelConfirmPopup;
import com.weichai.app.page.home.viewmodel.StationDetailsVM;

import dev.utils.app.AppUtils;
import dev.utils.app.ScreenUtils;
import dev.utils.app.SizeUtils;

/**
 * 服务站详情
 */
public class StationDetailsFragment extends BaseFragment<FragmentStationDetailsBinding, StationDetailsVM> {
    private TextureMapView mapView;
    private BaiduMap baiduMap;
    private LatLng point;
    private GpsUtil gpsUtil;

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        mapView = null;
        if (null != gpsUtil) {
            gpsUtil.onDestroy();
        }
        super.onDestroy();
    }

    @Override
    protected void initView() {
        gpsUtil = new GpsUtil(mActivity);
        gpsUtil.setGPSInterface(new GpsUtil.GPSInterface() {
            @Override
            public void gpsSwitchState(boolean gpsOpen) {
                if (gpsOpen) {
                    mViewBinding.clLocationOff.setVisibility(View.GONE);
                } else {
                    mViewBinding.clLocationOff.setVisibility(View.VISIBLE);
                }
            }
        });
        if (!GpsUtil.gpsIsOpen(mActivity)) {
            mViewBinding.clLocationOff.setVisibility(View.VISIBLE);
        }
        mViewBinding.tvOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                AppUtils.startActivity(intent);
            }
        });
        mViewBinding.ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewBinding.clLocationOff.setVisibility(View.GONE);
            }
        });

        mViewBinding.ivPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GpsUtil.gpsIsOpen(mActivity)) {
                    // 开始定位
                } else {
                    // 弹框
                    CancelConfirmPopup cancelConfirmPopup = new CancelConfirmPopup(mActivity);
                    cancelConfirmPopup.showAlert(new CancelConfirmPopup.AlertHint("定位服务未开启", "请在设置中开启定位设置", "暂不",
                            "去设置", true, new CancelConfirmPopup.OnConfirmListener() {
                        @Override
                        public void onConfirm() {
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            AppUtils.startActivity(intent);
                        }

                        @Override
                        public void onCancel() {

                        }
                    }));
                }
            }
        });
        mapView = mViewBinding.mapView;
        baiduMap = mViewBinding.mapView.getMap();
        if (null != getArguments()) {
            mViewModel.serviceStationEntity.setValue(getArguments().getParcelable("serviceStationEntity"));
        }
        mViewBinding.clTitleBar.setPadding(SizeUtils.dipConvertPx(16),
                mViewBinding.clTitleBar.getTop() + ScreenUtils.getStatusBarHeight() +
                        SizeUtils.dipConvertPx(2),
                SizeUtils.dipConvertPx(16), SizeUtils.dipConvertPx(2));

        //定义Maker坐标点
        if (null != mViewModel.serviceStationEntity.getValue()) {
            ServiceStationEntity serviceStationEntity = mViewModel.serviceStationEntity.getValue();
            point = new LatLng(Double.parseDouble(serviceStationEntity.dimension),
                    Double.parseDouble(serviceStationEntity.longitude));

        }
//构建Marker图标
        BitmapDescriptor bitmap = BitmapDescriptorFactory
                .fromResource(R.mipmap.ic_service_station_marker_normal);

        BitmapDescriptor bitmapSelect = BitmapDescriptorFactory
                .fromResource(R.mipmap.ic_service_station_marker_selected);
//构建MarkerOption，用于在地图上添加Marker
        Bundle bundle = new Bundle();
        bundle.putString("title", "点击marker");
        bundle.putString("address", "点击marker");
        OverlayOptions overlayOptions = new MarkerOptions()
                .position(point)
                .perspective(true)
                .extraInfo(bundle)
                .icon(bitmapSelect);
//在地图上添加Marker，并显示
        baiduMap.addOverlay(overlayOptions);

        baiduMap.setOnMapLoadedCallback(new BaiduMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                MapStatus mapStatus = new MapStatus.Builder().target(point).build();
                baiduMap.setMapStatus(MapStatusUpdateFactory.newMapStatus(mapStatus));
            }
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_station_details;
    }
}
