package com.weichai.app.page.home.adapter;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.chad.library.adapter.base.BaseDelegateMultiAdapter;
import com.chad.library.adapter.base.delegate.BaseMultiTypeDelegate;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.databinding.ItemCityBinding;
import com.weichai.app.databinding.ItemCurrentCityBinding;
import com.weichai.app.views.cityselect.model.CityInfoModel;

import java.util.List;

public class CitySelectAdapter extends BaseDelegateMultiAdapter<CityInfoModel, BaseViewHolder> {
    public static final int CURRENT_CITY = 1;
    public static final int CITY = 3;

    public CitySelectAdapter() {
        setMultiTypeDelegate(new BaseMultiTypeDelegate<CityInfoModel>() {
            @Override
            public int getItemType(@NonNull List<? extends CityInfoModel> list, int position) {
                return list.get(position).getType();
            }
        });
        getMultiTypeDelegate().addItemType(CURRENT_CITY, R.layout.item_current_city)
                .addItemType(CITY, R.layout.item_city);
    }

    @Override
    protected void onItemViewHolderCreated(@NonNull BaseViewHolder viewHolder, int viewType) {
        DataBindingUtil.bind(viewHolder.itemView);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, CityInfoModel cityInfoModel) {
        if (null == cityInfoModel) {
            return;
        }
        switch (baseViewHolder.getItemViewType()) {
            case CURRENT_CITY:
                ItemCurrentCityBinding currentCityBinding = baseViewHolder.getBinding();
                if (null != currentCityBinding) {
                    currentCityBinding.tvCurrentCity.setText(cityInfoModel.getCityName());
                }
                break;
            case CITY:
                ItemCityBinding cityBinding = baseViewHolder.getBinding();
                if (null != cityBinding) {
                    cityBinding.tvCity.setText(cityInfoModel.getCityName());
                }
                break;
            default:
                break;
        }
    }
}
