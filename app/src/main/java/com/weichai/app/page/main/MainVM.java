package com.weichai.app.page.main;

import androidx.databinding.ObservableField;
import androidx.fragment.app.Fragment;

import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.page.home.view.HomeFragment;
import com.weichai.app.page.mine.MineFragment;
import com.weichai.app.page.service.view.ServiceFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: MainVM
 * @Description:
 * @Author: 祖安
 * @Date: 2021/8/31 10:35 上午
 */
public class MainVM extends BaseViewModel {
    public ObservableField<List<Fragment>> fragmentList = new ObservableField<>();

    public MainVM() {
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(new HomeFragment());
        fragments.add(new MineFragment());
        fragments.add(new ServiceFragment());
        fragments.add(new MineFragment());
        fragmentList.set(fragments);
    }


}
