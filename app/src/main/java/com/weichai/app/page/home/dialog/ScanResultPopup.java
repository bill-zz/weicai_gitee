package com.weichai.app.page.home.dialog;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.weichai.app.R;

import razerdp.basepopup.BasePopupWindow;

/**
 * 扫描发动机二维码结果弹框
 */
public class ScanResultPopup extends BasePopupWindow implements View.OnClickListener {
    private ImageView ivClose;
    private TextView tvRepairImmediately, tvAppointmentMaintenance, tvEditEngine;
    private OnClickListener onClickListener;

    public ScanResultPopup(Context context) {
        super(context);
        setContentView(R.layout.popup_scan_result);
        setPopupGravity(Gravity.CENTER);
        showPopupWindow();
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        ivClose = findViewById(R.id.iv_close);
        tvRepairImmediately = findViewById(R.id.tv_repair_immediately);
        tvAppointmentMaintenance = findViewById(R.id.tv_appointment_maintenance);
        tvEditEngine = findViewById(R.id.tv_edit_engine);
    }

    @Override
    public void onShowing() {
        super.onShowing();
        ivClose.setOnClickListener(this);
        tvRepairImmediately.setOnClickListener(this);
        tvAppointmentMaintenance.setOnClickListener(this);
        tvEditEngine.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (null != onClickListener) {
            onClickListener.click(v);
        }
    }

    public interface OnClickListener {
        void click(View v);
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }
}
