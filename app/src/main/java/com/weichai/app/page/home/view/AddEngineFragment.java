package com.weichai.app.page.home.view;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.bean.DictRes;
import com.weichai.app.databinding.FragmentAddEngineBinding;
import com.weichai.app.page.home.viewmodel.AddEngineVM;
import com.weichai.app.views.dialog.SelectItemDialog;

public class AddEngineFragment extends BaseFragment<FragmentAddEngineBinding, AddEngineVM> {

    @Override
    protected void initView() {
        mViewBinding.setFg(this);
    }

    @Override
    protected void initData() {

    }

    public void selectIndustry() {
        SelectItemDialog dialog = new SelectItemDialog(requireContext());
        dialog.setTitle("选择所属行业");
        dialog.show();
        dialog.setClickOkCallback(dictRes -> {
//            mViewModel.addEngineReq.setIndustryType(dictRes.getType());
        });
    }

    public void selectCarUse() {
        SelectItemDialog dialog = new SelectItemDialog(requireContext());
        dialog.setTitle("选择车辆用途");
        dialog.show();
        dialog.setClickOkCallback(dictRes -> {
//            mViewModel.addEngineReq.setCarPurpose(dictRes.getType());
        });
    }


    public void selectCarBrand() {
        SelectItemDialog dialog = new SelectItemDialog(requireContext());
        dialog.setTitle("选择整车品牌");
        dialog.show();
        dialog.setClickOkCallback(dictRes -> {

        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_add_engine;
    }
}
