package com.weichai.app.page.home.adapter;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseDelegateMultiAdapter;
import com.chad.library.adapter.base.delegate.BaseMultiTypeDelegate;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.arch.utils.StringUtil;
import com.weichai.app.bean.SearchEntranceEntity;
import com.weichai.app.databinding.ItemSearchEntranceBinding;
import com.weichai.app.databinding.ItemSearchTitleBinding;

import java.util.List;

public class SearchEntranceAdapter extends BaseDelegateMultiAdapter<SearchEntranceEntity, BaseViewHolder> {
    public static final int TITLE = 1;
    public static final int ENTRANCE = 2;

    public SearchEntranceAdapter() {
        setMultiTypeDelegate(new BaseMultiTypeDelegate<SearchEntranceEntity>() {
            @Override
            public int getItemType(@NonNull List<? extends SearchEntranceEntity> list, int position) {
                if (position == 0) {
                    return TITLE;
                } else {
                    return ENTRANCE;
                }
            }
        });
        getMultiTypeDelegate().addItemType(TITLE, R.layout.item_search_title)
                .addItemType(ENTRANCE, R.layout.item_search_entrance);
    }

    @Override
    protected void onItemViewHolderCreated(@NonNull BaseViewHolder viewHolder, int viewType) {
        DataBindingUtil.bind(viewHolder.itemView);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, SearchEntranceEntity searchEntranceEntity) {
        if (null == searchEntranceEntity) {
            return;
        }
        switch (baseViewHolder.getItemViewType()) {
            case TITLE:
                ItemSearchTitleBinding searchTitleBinding = baseViewHolder.getBinding();
                if (null != searchTitleBinding) {
                    searchTitleBinding.tvTitle.setText("功能入口");
                }
                break;
            case ENTRANCE:
                ItemSearchEntranceBinding searchEntranceBinding = baseViewHolder.getBinding();
                if (null != searchEntranceBinding) {
                    Glide.with(getContext())
                            .load(StringUtil.getUrl(searchEntranceEntity.functionPicture))
                            .error(R.mipmap.ic_app_logo)
                            .into(searchEntranceBinding.ivIcon);
                    searchEntranceBinding.tvName.setText(searchEntranceEntity.functionName);
                }
                break;
            default:
                break;
        }
    }
}
