package com.weichai.app.page.home.adapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.databinding.ItemMaintenanceProgressBinding;

import dev.utils.app.ResourceUtils;
import dev.utils.app.ScreenUtils;

public class MaintenanceProgressAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    private static final int TYPE = 3;

    public MaintenanceProgressAdapter() {
        super(R.layout.item_maintenance_progress);
    }

    @Override
    protected void onItemViewHolderCreated(@NonNull BaseViewHolder viewHolder, int viewType) {
        DataBindingUtil.bind(viewHolder.itemView);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, String s) {
        ItemMaintenanceProgressBinding binding = baseViewHolder.getBinding();
        if (null != binding) {
            ViewGroup.LayoutParams layoutParams = binding.getRoot().getLayoutParams();
            layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            layoutParams.width = (ScreenUtils.getScreenWidth() - ResourceUtils.getDimensionInt(R.dimen.dp_16) * 2) / 5;
            binding.getRoot().setLayoutParams(layoutParams);
            binding.tvName.setText(s);
            if (TYPE == baseViewHolder.getAdapterPosition()) {
                binding.tvName.setTextColor(ResourceUtils.getColor(R.color.color_FFFF4242));
                binding.ivIcon.setImageResource(R.mipmap.ic_progress_selected);
            } else {
                binding.tvName.setTextColor(ResourceUtils.getColor(R.color.color_D9000000));
                binding.ivIcon.setImageResource(R.mipmap.ic_progress_normal);
            }
        }
    }
}
