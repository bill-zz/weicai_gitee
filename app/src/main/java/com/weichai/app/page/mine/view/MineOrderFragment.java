package com.weichai.app.page.mine.view;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.google.android.material.tabs.TabLayoutMediator;
import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentMineMsgBinding;
import com.weichai.app.databinding.FragmentMineOrderBinding;
import com.weichai.app.page.mine.viewmodel.MineMsgVM;
import com.weichai.app.page.mine.viewmodel.MineOrderVM;

import java.util.ArrayList;
import java.util.List;

/**
 * create by bill on 6.12.21
 */
public class MineOrderFragment extends BaseFragment<FragmentMineOrderBinding, MineOrderVM> {
    @Override
    protected void initView() {

        List<String> tabList = new ArrayList<>();
        tabList.add("全部");
        tabList.add("预约中");
        tabList.add("预约成功");
        tabList.add("预约取消");

        mViewBinding.tab.addTab(mViewBinding.tab.newTab().setText(tabList.get(0)));
        mViewBinding.tab.addTab(mViewBinding.tab.newTab().setText(tabList.get(1)));
        mViewBinding.tab.addTab(mViewBinding.tab.newTab().setText(tabList.get(2)));
        mViewBinding.tab.addTab(mViewBinding.tab.newTab().setText(tabList.get(3)));

        MineOrderChildFragment all = new MineOrderChildFragment();
        MineOrderChildFragment orderIng = new MineOrderChildFragment();
        MineOrderChildFragment orderOk = new MineOrderChildFragment();
        MineOrderChildFragment orderError = new MineOrderChildFragment();

        List<Fragment> fgList = new ArrayList<>();
        fgList.add(all);
        fgList.add(orderIng);
        fgList.add(orderOk);
        fgList.add(orderError);

        mViewBinding.vp.setAdapter(new FragmentStateAdapter(getChildFragmentManager(), getLifecycle()) {
            @NonNull
            @Override
            public Fragment createFragment(int position) {
                return fgList.get(position);
            }

            @Override
            public int getItemCount() {
                return fgList.size();
            }
        });

        new TabLayoutMediator(
                mViewBinding.tab,
                mViewBinding.vp,
                (tab1, position) -> {
                    tab1.setText(tabList.get(position));
                }
        ).attach();

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine_order;
    }
}
